# name: Discourse Mermaid Diagram plugin
# about: Draw a diagram
# version: 0.1
# authors: Nathan Skerl

register_asset "javascripts/mermaid_diagram_dialect.js", :server_side
register_asset "javascripts/raphael-min.js"
register_asset "stylesheets/mermaid.css"
register_asset "javascripts/mermaid-min.js"