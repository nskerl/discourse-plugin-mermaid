(function() {
	function insertDiagram(_, contents) {
		return "<div class='mermaid'>" + contents + "</div>";
	}

	function replaceDiagrams(text) {
		text = text || "";
		text = text.replace(/\[seqdiagram\]((?:.|\r?\n)*?)\[\/seqdiagram\]/igm, insertDiagram);
		return text;
	}
  
	Discourse.Dialect.addPreProcessor(function(text) {
		if (Discourse.SiteSettings.enable_mermaid_diagram_plugin) {
		  text = replaceDiagrams(text);
		}
		return text;
	});
	Discourse.Markdown.whiteListTag('div', 'class', /^mermaid$/igm);
})();
