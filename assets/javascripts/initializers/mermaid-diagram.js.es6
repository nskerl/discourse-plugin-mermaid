import { withPluginApi, decorateCooked } from 'discourse/lib/plugin-api';

function convertToDiagram($elem) {
	mermaid.init();
}

export default {
	name: 'discourse-mermaid-diagram',
	initialize(container) {	
		const siteSettings = container.lookup('site-settings:main');
		if (siteSettings.enable_mermaid_diagram_plugin) {
			withPluginApi('0.1', api => {
				api.decorateCooked(convertToDiagram);
			});
		}
	}
}
